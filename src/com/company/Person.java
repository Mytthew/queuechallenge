package com.company;

public class Person {
    private String name;
    private String lastName;
    private Person prev;

    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public Person getPrev() {
        return prev;
    }

    public void setPrev(Person prev) {
        this.prev = prev;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", prev=" + prev +
                '}';
    }
}
