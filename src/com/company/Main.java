package com.company;

public class Main {

	public static void main(String[] args) {
		Queue queue = new Queue();
		Person person1 = new Person("Mateusz", "D");
		Person person2 = new Person("Andrzej", "C");
		Person person3 = new Person("Jan", "K");
		queue.add(person1);
		queue.add(person2);
		queue.add(person3);
		System.out.println("Peek: " + queue.peek());
		System.out.println("Pop: " + queue.pop());
		System.out.println(person3.toString());
	}
}

