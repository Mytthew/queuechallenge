package com.company;

public class Queue {
    private Person last = null;

    public void add(Person newPerson) {
        if (last == null) {
            last = newPerson;
        } else {
            newPerson.setPrev(last);
            last = newPerson;
        }
    }

    public Person peek() {
        Person walking = last;
        while (walking.getPrev() != null) {
            walking = walking.getPrev();
            return walking.getPrev();
        }
        return null;
    }

    public Person pop() {
        Person walking = last;
        while (walking.getPrev() != null) {
            walking.getPrev();
        }
        return walking;
    }
}
